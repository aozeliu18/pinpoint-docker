#!/bin/bash
set -e
set -x

#sed -i "/profiler.transport.module=/ s/=.*/=${PROFILER_TRANSPORT_MODULE}/" /pinpoint-agent/pinpoint.config
sed -i "/profiler.transport.module=/ s/=.*/=${PROFILER_TRANSPORT_MODULE}/" /pinpoint-agent/profiles/${SPRING_PROFILES}/pinpoint.config

sed -i "/profiler.collector.ip=/ s/=.*/=${COLLECTOR_IP}/" /pinpoint-agent/profiles/${SPRING_PROFILES}/pinpoint.config
sed -i "/profiler.collector.tcp.port=/ s/=.*/=${COLLECTOR_TCP_PORT}/" /pinpoint-agent/pinpoint-root.config
sed -i "/profiler.collector.stat.port=/ s/=.*/=${COLLECTOR_STAT_PORT}/" /pinpoint-agent/pinpoint-root.config
sed -i "/profiler.collector.span.port=/ s/=.*/=${COLLECTOR_SPAN_PORT}/" /pinpoint-agent/pinpoint-root.config

#sed -i "/profiler.transport.grpc.collector.ip=/ s/=.*/=${COLLECTOR_IP}/" /pinpoint-agent/pinpoint.config
sed -i "/profiler.transport.grpc.collector.ip=/ s/=.*/=${COLLECTOR_IP}/" /pinpoint-agent/profiles/${SPRING_PROFILES}/pinpoint.config
sed -i "/profiler.transport.grpc.agent.collector.port=/ s/=.*/=${PROFILER_TRANSPORT_AGENT_COLLECTOR_PORT}/" /pinpoint-agent/pinpoint-root.config
sed -i "/profiler.transport.grpc.metadata.collector.port=/ s/=.*/=${PROFILER_TRANSPORT_METADATA_COLLECTOR_PORT}/" /pinpoint-agent/pinpoint-root.config
sed -i "/profiler.transport.grpc.stat.collector.port=/ s/=.*/=${PROFILER_TRANSPORT_STAT_COLLECTOR_PORT}/" /pinpoint-agent/pinpoint-root.config
sed -i "/profiler.transport.grpc.span.collector.port=/ s/=.*/=${PROFILER_TRANSPORT_SPAN_COLLECTOR_PORT}/" /pinpoint-agent/pinpoint-root.config
sed -i "/profiler.sampling.rate=/ s/=.*/=${PROFILER_SAMPLING_RATE}/" /pinpoint-agent/profiles/${SPRING_PROFILES}/pinpoint.config

sed -i "/webrts.agent.base.packages=/ s/=.*/=${WEBRTS_AGENT_BASE_PACKAGES}/" /pinpoint-agent/profiles/${SPRING_PROFILES}/pinpoint.config
sed -i "/webrts.only.public=/ s/=.*/=${WEBRTS_ONLY_PUBLIC}/" /pinpoint-agent/profiles/${SPRING_PROFILES}/pinpoint.config
sed -i "/webrts.application.type=/ s/=.*/=${WEBRTS_APPLICATION_TYPE}/" /pinpoint-agent/profiles/${SPRING_PROFILES}/pinpoint.config
sed -i "/webrts.application.base.dir=/ s#=.*#=${WEBRTS_APPLICATION_BASE_DIR}#" /pinpoint-agent/profiles/${SPRING_PROFILES}/pinpoint.config
sed -i "/webrts.only.constructor=/ s/=.*/=${WEBRTS_ONLY_CONSTRUCTOR}/" /pinpoint-agent/profiles/${SPRING_PROFILES}/pinpoint.config
sed -i "/webrts.initrecorder.enable=/ s/=.*/=${WEBRTS_INIT_RECORDER_ENABLE}/" /pinpoint-agent/profiles/${SPRING_PROFILES}/pinpoint.config
sed -i "/webrts.dump.classname=/ s/=.*/=${WEBRTS_DUMP_CLASSNAME}/" /pinpoint-agent/profiles/${SPRING_PROFILES}/pinpoint.config


sed -i "/Root level=/ s/=.*/=\"${DEBUG_LEVEL}\">/g" /pinpoint-agent/profiles/${SPRING_PROFILES}/log4j2.xml

exec "$@"
