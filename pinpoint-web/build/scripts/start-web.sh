#!/bin/bash
set -e
set -x

exec java ${JAVA_OPTS} -jar /pinpoint/pinpoint-web-boot.jar --spring.config.additional-location=/pinpoint/config/pinpoint-web.properties